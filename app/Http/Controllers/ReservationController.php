<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Mail\ReservationMail;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function reserve(Request $request) {
        $date_start = $request->date_start;
        $stand_id = $request->stand_id;
        return view('reservations.create', compact('date_start', 'stand_id'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date_start = \DateTime::createFromFormat('d-m-Y', $request->date_start);
        $date_end = \DateTime::createFromFormat('d-m-Y', $request->date_end);

        $reservation = new Reservation;
        
        $reservation->company = $request->company;
        $reservation->rfc = $request->rfc;
        $reservation->email = $request->email;
        $reservation->date_start = $date_start->format('Y-m-d H:i:s ');
        $reservation->date_end = $date_end->format('Y-m-d H:i:s ');
        $reservation->description = $request->description;
        $reservation->stand_id = $request->stand_id;

        $reservation->save();

        //\Mail::to($reservation->email)->send(new ReservationMail());

        return redirect()->action('StandController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }
}
