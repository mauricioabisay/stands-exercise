<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('company');
            $table->string('rfc');
            $table->string('email');
            $table->date('date_start');
            $table->date('date_end');
            $table->mediumText('description');

            $table->unsignedInteger('stand_id');
            $table->foreign('stand_id')
                ->references('id')->on('stands')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
