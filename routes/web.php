<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->action('StandController@index');
});

Route::get('/available/{stand}/{date}', function($stand, $date) {
    $reservations = App\Reservation::whereRaw(' (stand_id='.$stand.' AND date_start<='.$date.' AND date_end>='.$date.')')
        ->count();
    return response()->json([
        'reservations' => $reservations
    ]);
});

Route::post('/reserve', 'ReservationController@reserve');
Route::post('/reserve/store', 'ReservationController@store');

Route::resources([
    'stands' => 'StandController'
]);