<!DOCTYPE html>
<html>
<head>
	<title>Programming Exercise</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<style>
		.status {
			display: none;
		}
	</style>
</head>
<body>
	<div class="container-fluid" style="margin-top:60px">
		@yield('content')
	</div>
	@include('common.footer')
</body>
</html>