<footer>Mauricio Abisay López Velázquez</footer>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" 
    integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" 
    crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" 
    integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" 
    crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
	integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
	crossorigin="anonymous"></script>

<script>
    jQuery('.datepicker').datepicker({
        defaultDate: "+0d",
        minDate: "+0d",
        dateFormat: "dd-mm-yy"
    });
    jQuery('.status-datepicker').change(function(){
        var stand = jQuery(this).attr('stand');
        var dateTarget = jQuery(this).val();
        var url = "{{ url('/available/') }}/" + stand + "/" + dateTarget;
        console.log(url);
        jQuery.ajax({
            url: "{{ url('/available/') }}/" + stand + "/" + dateTarget,
            success: function(data) {
                jQuery('#'+stand+'-occupied').css('display', 'none');
                jQuery('#'+stand+'-vacant').css('display', 'none');
                if ( data.reservations > 0 ) {
                    jQuery('#'+stand+'-occupied').css('display', 'block');
                } else {
                    jQuery('#'+stand+'-vacant').css('display', 'block');
                }
            },
            dataType: 'json'
        });
    });
</script>