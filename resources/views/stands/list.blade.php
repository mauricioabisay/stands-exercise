@extends('common.main')

@section('content')
<table class="table">
    <thead>
        <tr>
            <th>Número de Stand</th>
            <th>Verificar</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>
        @foreach ( $stands as $s )
        <form action="{{ action('ReservationController@reserve') }}" method="post">
            @csrf
            <input type="hidden" name="stand_id" value="{{ $s->id }}"/>
            <tr>
                <td>{{ $s->id }}</td>
                <td>
                    <small>Seleccione una fecha para ver disponibilidad:</small>
                    <input type="text" 
                        class="datepicker status-datepicker" 
                        stand="{{ $s->id }}" 
                        name="date_start"/>
                </td>
                <td>
                    <span id="{{$s->id}}-vacant" class="status">Libre <input type="submit" value="Reservar" class="btn btn-primary"/></span>
                    <span id="{{$s->id}}-occupied" class="status">Ocupado</span>
                </td>
            </tr>
        </form>
        @endforeach
    </tbody>
</table>