@extends('common.main')

@section('content')
<form action="{{ action('ReservationController@store') }}" method="post">
    @csrf
    <input type="text" name="stand_id" value="{{ $stand_id }}"/>
    <div class="form-group">
        <label>Compañia</label>
        <input type="text" class="form-control" name="company">
    </div>
    <div class="form-group">
        <label>R.F.C.</label>
        <input type="text" class="form-control" name="rfc">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" name="email">
    </div>
    <div class="form-group">
        <label>Día inicio</label>
        <input type="text" 
            class="datepicker form-control" 
            name="date_start" 
            value="{{ $date_start }}">
    </div>
    <div class="form-group">
        <label>Día fin</label>
        <input type="text" class="datepicker form-control" name="date_end">
    </div>
    <div class="form-group">
        <label>Descripción</label>
        <textarea class="form-control" name="description"></textarea>
    </div>
    <div class="form-group">
        <input type="submit" value="Reservar"/>
    </div>
</form>