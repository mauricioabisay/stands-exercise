# Programming exercise

## Instructions

1. Create a dashboard to visualize 6 stands info
2. If a stand is free, the dashboard should show an option to Reserve such stand.
3. The reservation info includes:
- Company's name
- R.F.C.
- Start date
- End date
- Description
- Logo
 4. After reservation is completed an email should sended.